package internal

import (
	"context"
	"sync"
	"time"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/jobd/fleeting/fleeting"
	"gitlab.com/jobd/fleeting/fleeting/provider"
)

type Instance struct {
	instance fleeting.Instance
	logger   hclog.Logger
	info     provider.ConnectInfo

	capacityPerInstance int
	maxUseCount         int

	mu        sync.Mutex
	acquired  int
	used      int
	preparing bool
	ready     bool
	removing  bool
}

func New(instance fleeting.Instance, logger hclog.Logger, capacityPerInstance, maxUseCount int) *Instance {
	logger = logger.With("instance", instance.ID())

	return &Instance{
		instance:            instance,
		logger:              logger,
		capacityPerInstance: capacityPerInstance,
		maxUseCount:         maxUseCount,
	}
}

func (inst *Instance) ID() string {
	return inst.instance.ID()
}

func (inst *Instance) Acquire() bool {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	switch {
	case inst.removing, !inst.ready:
		return false

	case inst.acquired >= inst.capacityPerInstance:
		return false

	case inst.maxUseCount > 0 && inst.used >= inst.maxUseCount:
		return false
	}

	inst.used++
	inst.acquired++

	return true
}

func (inst *Instance) Relinquish() {
	inst.mu.Lock()
	inst.acquired--
	if inst.acquired < 0 {
		panic("acquired below minimum of zero")
	}

	free := inst.used > 0 && inst.used >= inst.maxUseCount
	inst.mu.Unlock()

	if free {
		inst.Free(0)
	}
}

// Capacity returns the acquired and unavailable capacity of an instance.
//
// The unavailable capacity is typically 0, indicating that all capacity is
// available (other than whatever the acquired capacity is).
//
// The unavailable capacity is above zero when a max used count is configured,
// and the used count is affecting the capacity:
// - if you have a capacity of 5 and a used count of 1/100, you still have a
//   capacity of 5. The unavailable capacity is therefore 0.
// - if you have a capacity of 5 and a used count of 96/100, you now only
//   have a capacity of 4. The unavailable capacity is therefore 1.
func (inst *Instance) Capacity() (acquired, unavailable int) {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	if !inst.removing {
		remainingUses := inst.maxUseCount - inst.used
		if remainingUses < inst.capacityPerInstance {
			unavailable = inst.capacityPerInstance - remainingUses
			unavailable -= inst.acquired
		}
	}

	return inst.acquired, unavailable
}

func (inst *Instance) IsReady() bool {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	return inst.ready
}

func (inst *Instance) Run(fn func(info provider.ConnectInfo) error) error {
	return fn(inst.info)
}

func (inst *Instance) ConnectInfo() provider.ConnectInfo {
	return inst.info
}

func (inst *Instance) Prepare(ctx context.Context, readyUpFunc func(id string, info provider.ConnectInfo) error) {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	if inst.removing {
		return
	}

	if inst.preparing || inst.ready {
		return
	}

	inst.preparing = true

	go func() {
		defer func() {
			inst.mu.Lock()
			inst.preparing = false
			inst.mu.Unlock()
		}()

		start := time.Now()

		if !inst.prepareWait(ctx) {
			return
		}

		info, err := inst.prepareConnect(ctx)
		if err != nil {
			inst.logger.Error("connection preparation failed", "err", err)
			inst.Free(0)
			return
		}
		inst.info = info

		if readyUpFunc != nil {
			if err := readyUpFunc(inst.ID(), info); err != nil {
				inst.logger.Error("ready up preparation failed", "err", err)
				inst.Free(0)
				return
			}
		}

		inst.mu.Lock()
		inst.ready = true
		inst.mu.Unlock()

		inst.logger.Info("ready", "took", time.Since(start))
	}()
}

func (inst *Instance) prepareWait(ctx context.Context) bool {
	ctx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()

	return inst.instance.ReadyWait(ctx)
}

func (inst *Instance) prepareConnect(ctx context.Context) (provider.ConnectInfo, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()

	return inst.instance.ConnectInfo(ctx)
}

// Free marks an instance for deletion if certain rules are matched.
func (inst *Instance) Free(minIdleTime time.Duration) bool {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	// don't mark acquired instanced for deletion
	if inst.acquired > 0 {
		return false
	}

	// keep the instance if it hasn't yet exceeded the minimum idle time
	if time.Since(inst.instance.ProvisionedAt()) < minIdleTime {
		return false
	}

	inst.instance.Delete()
	inst.removing = true

	return true
}
