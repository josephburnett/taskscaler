package internal

import (
	"context"
	"strconv"
	"testing"
	"time"

	"gitlab.com/jobd/fleeting/fleeting"
	"gitlab.com/jobd/fleeting/fleeting/provider"
)

type dummyInstance struct {
	id            string
	provisionedAt time.Time
}

func (i *dummyInstance) ID() string {
	return i.id
}

func (i *dummyInstance) State() provider.State {
	return provider.StateDeleted
}

func (i *dummyInstance) Delete() {
}

func (i *dummyInstance) Cause() fleeting.Cause {
	return fleeting.CausePreexisted
}

func (i *dummyInstance) ReadyWait(context.Context) bool {
	return true
}

func (i *dummyInstance) ConnectInfo(context.Context) (provider.ConnectInfo, error) {
	return provider.ConnectInfo{}, nil
}

func (i *dummyInstance) RequestedAt() time.Time {
	return time.Now()
}

func (i *dummyInstance) ProvisionedAt() time.Time {
	return i.provisionedAt
}

func (i *dummyInstance) UpdatedAt() time.Time {
	return time.Now()
}

func (i *dummyInstance) DeletedAt() time.Time {
	return time.Now()
}

func instanceInsertion(m *List, insts []*Instance) {
	for idx := range insts {
		m.Add(insts[idx])
	}
}

func instanceDeletion(m *List, insts []*Instance) {
	for idx := range insts {
		m.Delete(insts[idx].ID())
	}
}

func BenchmarkInsertionDeletion(b *testing.B) {
	var insts []*Instance
	for i := 0; i < 10000; i++ {
		insts = append(insts, &Instance{instance: &dummyInstance{
			id:            strconv.Itoa(i),
			provisionedAt: time.Now().Add(time.Duration(i) * time.Hour),
		}})
	}
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		var instances List
		instanceInsertion(&instances, insts)
		instanceDeletion(&instances, insts)
	}
}

func BenchmarkInsertion(b *testing.B) {
	var insts []*Instance
	for i := 0; i < 10000; i++ {
		insts = append(insts, &Instance{instance: &dummyInstance{
			id:            strconv.Itoa(i),
			provisionedAt: time.Now().Add(time.Duration(i) * time.Hour),
		}})
	}
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		var instances List
		instanceInsertion(&instances, insts)
	}
}

func BenchmarkList(b *testing.B) {
	var insts []*Instance
	for i := 0; i < 10000; i++ {
		insts = append(insts, &Instance{instance: &dummyInstance{
			id:            strconv.Itoa(i),
			provisionedAt: time.Now().Add(time.Duration(i) * time.Hour),
		}})
	}

	var instances List
	instanceInsertion(&instances, insts)

	b.ResetTimer()
	b.ReportAllocs()

	go func() {
		for {
			instances.Add(&Instance{instance: &dummyInstance{}})
		}
	}()

	for i := 0; i < b.N; i++ {
		instances.List()
	}
}
