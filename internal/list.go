package internal

import "sync"

// List indexes instances by ID and maintains insertion order for quick access
// to the oldest instances.
type List struct {
	mu    sync.Mutex
	index map[string]*Instance
	list  []*Instance
}

func (a *List) Get(id string) *Instance {
	a.mu.Lock()
	defer a.mu.Unlock()

	if a.index == nil {
		return nil
	}
	return a.index[id]
}

func (a *List) Add(inst *Instance) {
	a.mu.Lock()
	defer a.mu.Unlock()

	if a.index == nil {
		a.index = make(map[string]*Instance)
	}

	a.index[inst.ID()] = inst
	a.list = append(a.list, inst)
}

func (a *List) Delete(id string) {
	a.mu.Lock()
	defer a.mu.Unlock()

	delete(a.index, id)
	for idx := range a.list {
		if a.list[idx].ID() == id {
			a.list = append(a.list[:idx], a.list[idx+1:]...)
			return
		}
	}
}

// Capacity returns the idle, acquired and unavailable capacity as a total of
// all available instances.
func (a *List) Capacity() (idle, acquired, unavailable int) {
	a.mu.Lock()
	defer a.mu.Unlock()

	for _, instance := range a.list {
		x, y := instance.Capacity()
		acquired += x
		unavailable += y

		if instance.IsReady() {
			idle += instance.capacityPerInstance - x - y
		}
	}

	return idle, acquired, unavailable
}

func (a *List) List() []*Instance {
	a.mu.Lock()
	defer a.mu.Unlock()

	list := make([]*Instance, len(a.list))
	copy(list, a.list)

	return list
}
