package taskscaler

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/jobd/fleeting/fleeting"
	"gitlab.com/jobd/fleeting/fleeting/provider"
	"gitlab.com/jobd/fleeting/taskscaler/internal"
	"gitlab.com/jobd/fleeting/taskscaler/internal/capacity"
)

type Taskscaler struct {
	opts        options
	group       provider.InstanceGroup
	provisioner *fleeting.Provisioner
	instances   internal.List

	mu           sync.Mutex
	acquisitions map[string]*Aquisition
	pending      int
	schedules    schedules
	active       Schedule
}

type Aquisition struct {
	id         string
	info       provider.ConnectInfo
	relinquish func()
}

func (a *Aquisition) InstanceID() string {
	return a.id
}

func (a *Aquisition) InstanceConnectInfo() provider.ConnectInfo {
	return a.info
}

func New(ctx context.Context, group provider.InstanceGroup, options ...Option) (*Taskscaler, error) {
	opts, err := newOptions(options)
	if err != nil {
		return nil, fmt.Errorf("client option: %w", err)
	}

	ts := &Taskscaler{
		opts:         opts,
		group:        group,
		acquisitions: make(map[string]*Aquisition),
		active:       defaultSchedule,
	}

	return ts, ts.start(ctx)
}

func (ts *Taskscaler) start(ctx context.Context) error {
	opts := []fleeting.Option{
		fleeting.WithMaxSize(ts.opts.maxInstances),
		fleeting.WithInstanceGroupSettings(ts.opts.settings),
		fleeting.WithSubscriber(func(instances []fleeting.Instance) {
			ts.updates(ctx, instances)
		}),
	}

	pctx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()

	provisioner, err := fleeting.Init(pctx, ts.opts.logger, ts.group, opts...)
	if err != nil {
		return fmt.Errorf("initializing provisioner: %w", err)
	}

	ts.provisioner = provisioner
	go func() {
		defer provisioner.Shutdown(context.Background())

		for {
			if ctx.Err() != nil {
				return
			}

			time.Sleep(time.Second)
			ts.scale(ctx, ts.desired(ctx))
		}
	}()

	return nil
}

func (ts *Taskscaler) Schedule() Schedule {
	ts.mu.Lock()
	defer ts.mu.Unlock()

	return ts.active
}

func (ts *Taskscaler) Acquire(ctx context.Context, key string) (*Aquisition, error) {
	var instance *internal.Instance

	ts.mu.Lock()
	ts.pending++
	ts.mu.Unlock()

	defer func() {
		ts.mu.Lock()
		ts.pending--
		ts.mu.Unlock()
	}()

	for {
		if ctx.Err() != nil {
			return nil, ctx.Err()
		}

		instance = ts.acquire()
		if instance == nil {
			time.Sleep(ts.opts.acquireDelay)
			continue
		}
		break
	}

	if instance == nil {
		return nil, fmt.Errorf("no capacity")
	}

	ts.mu.Lock()
	defer ts.mu.Unlock()

	acq := &Aquisition{
		id:         instance.ID(),
		info:       instance.ConnectInfo(),
		relinquish: instance.Relinquish,
	}
	ts.acquisitions[key] = acq

	return acq, nil
}

func (ts *Taskscaler) acquire() *internal.Instance {
	for _, instance := range ts.instances.List() {
		if instance.Acquire() {
			return instance
		}
	}

	return nil
}

func (ts *Taskscaler) Get(key string) *Aquisition {
	ts.mu.Lock()
	defer ts.mu.Unlock()

	return ts.acquisitions[key]
}

func (ts *Taskscaler) Release(key string) {
	ts.mu.Lock()
	defer ts.mu.Unlock()

	acq, ok := ts.acquisitions[key]
	if !ok {
		return
	}

	acq.relinquish()
	delete(ts.acquisitions, key)
}

// Capacity returns available (immediate) capacity and potential (on-demand)
// capacity.
func (ts *Taskscaler) Capacity() (available int, potential int) {
	capacity := ts.provisioner.Capacity()

	var total int
	if capacity.Max > 0 {
		total = capacity.Max * ts.opts.capacityPerInstance
	}

	idle, acquired, unavailable := ts.instances.Capacity()
	max := acquired + unavailable

	return idle, total - max
}

func (ts *Taskscaler) updates(ctx context.Context, instances []fleeting.Instance) {
	for _, instance := range instances {
		// todo: for now we're deleting any pre-existing instance.
		// eventually, we'd like to store state, so that we can keep any instances
		// where the used count etc. is still valid.
		if instance.Cause() == fleeting.CausePreexisted {
			instance.Delete()
			continue
		}

		switch instance.State() {
		case provider.StateRunning:
			ts.mu.Lock()
			inst := ts.instances.Get(instance.ID())
			if inst == nil {
				inst = internal.New(instance, ts.opts.logger, ts.opts.capacityPerInstance, ts.opts.maxUseCount)
				ts.instances.Add(inst)
			}
			ts.mu.Unlock()

			inst.Prepare(ctx, ts.opts.upFn)

		case provider.StateDeleted, provider.StateTimeout:
			ts.instances.Delete(instance.ID())
		}
	}
}

func (ts *Taskscaler) desired(ctx context.Context) int {
	c := ts.provisioner.Capacity()
	_, acquired, unavailable := ts.instances.Capacity()

	ts.mu.Lock()
	pending := ts.pending
	ts.active = ts.schedules.active(time.Now())
	active := ts.active
	ts.mu.Unlock()

	return capacity.RequiredInstances(capacity.CapacityInfo{
		InstanceCount:       c.Creating + c.Running + c.Requested,
		MaxInstanceCount:    c.Max,
		Acquired:            acquired,
		UnavailableCapacity: unavailable,
		Pending:             pending,
		IdleCount:           active.IdleCount,
		ScaleFactor:         active.ScaleFactor,
		ScaleFactorLimit:    active.ScaleFactorLimit,
		CapacityPerInstance: ts.opts.capacityPerInstance,
	})
}

func (ts *Taskscaler) scale(ctx context.Context, n int) {
	if n == 0 {
		return
	}

	if n > 0 {
		ts.opts.logger.Debug("scale up", "n", n)
		ts.provisioner.Request(n)
		return
	}
	ts.opts.logger.Debug("scale down", "n", n)

	ts.mu.Lock()
	active := ts.active
	ts.mu.Unlock()

	for _, instance := range ts.instances.List() {
		if n == 0 {
			break
		}

		if instance.Free(time.Duration(active.IdleTime)) {
			n++
		}
	}
}
