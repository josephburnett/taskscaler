package taskscaler

import (
	"context"
	"testing"
	"time"

	dummy "gitlab.com/jobd/fleeting/fleeting/plugin/fleeting-plugin-dummy"
)

func BenchmarkSchedule(b *testing.B) {
	ts, err := New(context.Background(), &dummy.InstanceGroup{})
	if err != nil {
		b.Fatal(err)
	}

	err = ts.ConfigureSchedule(
		Schedule{
			Periods:   []string{"* * * * *"},
			IdleCount: 5,
		},
		Schedule{
			Periods:   []string{"* * * * mon,sun"},
			IdleCount: 10,
		},
		Schedule{
			Periods:   []string{"5 2 * * wed"},
			IdleCount: 20,
		},
	)
	if err != nil {
		b.Fatal(err)
	}

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ts.schedules.active(time.Date(2020, time.February, 1, 0, 0, 0, 0, time.Local))
	}
}

func TestSchedule(t *testing.T) {
	ts, err := New(context.Background(), &dummy.InstanceGroup{})
	if err != nil {
		t.Fatal(err)
	}

	err = ts.ConfigureSchedule(
		Schedule{
			Periods:   []string{"1 * * * *"},
			IdleCount: 5,
		},
		Schedule{
			Periods:   []string{"* * * * mon,sun"},
			IdleCount: 10,
		},
		Schedule{
			Periods:   []string{"5 2 * * wed"},
			IdleCount: 20,
		},
	)
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		date                 time.Time
		expectedMinIdleCount int
	}{
		{date: time.Date(2020, time.February, 5, 2, 5, 0, 0, time.UTC), expectedMinIdleCount: 20},
		{date: time.Date(2020, time.February, 3, 0, 0, 0, 0, time.UTC), expectedMinIdleCount: 10},
		{date: time.Date(2020, time.February, 3, 23, 59, 59, 0, time.UTC), expectedMinIdleCount: 10},
		{date: time.Date(2020, time.February, 5, 2, 1, 0, 0, time.UTC), expectedMinIdleCount: 5},
		{date: time.Date(2020, time.February, 5, 2, 8, 0, 0, time.UTC), expectedMinIdleCount: 0},
	}

	for _, tc := range tests {
		min := ts.schedules.active(tc.date).IdleCount
		if min != tc.expectedMinIdleCount {
			t.Errorf("expected %d, got %d for %s", tc.expectedMinIdleCount, min, tc.date)
		}
	}
}
